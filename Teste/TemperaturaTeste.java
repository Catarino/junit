package Teste;

import static org.junit.Assert.*;
import junit.framework.Assert;

import org.junit.Test;

import model.Temperatura;

public class TemperaturaTeste {


	@Test
	public void test() {
		Temperatura temperaturaTeste = new Temperatura();
		temperaturaTeste.setCelsius(30);
		Assert.assertEquals(300, temperaturaTeste.CelsiusParaKelvin(30), 0.01);
	}

}
