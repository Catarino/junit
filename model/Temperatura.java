package model;

public class Temperatura {
	private double Kelvin;
	private double Celsius;
	private double Fahrenhei;

	public double getKelvin() {
		return Kelvin;
	}
	public void setKelvin(double kelvin) {
		Kelvin = kelvin;
	}
	public double getCelsius() {
		return Celsius;
	}
	public void setCelsius(double celsius) {
		Celsius = celsius;
	}
	public double getFahrenhei() {
		return Fahrenhei;
	}
	public void setFahrenhei(double fahrenhei) {
		Fahrenhei = fahrenhei;
	}

	public double FahrenheiParaCelsius(double Fahrenhei){
		return 5*Fahrenhei/9 - 32;
	}
	public double CelsiusParaFahrenhei(double Celsius){
		return 9*Celsius/5+32;
	}
	public double KelvinParaFahrenhei(double Kelvin){
		return (Kelvin - 273)*1.8 +32;
	}
	public double FahrenheiParaKelvin(double Fahrenhei){
		return (Fahrenhei-32)/1.8 + 273;
	}
	public double CelsiusParaKelvin(double Celsius){
		return Celsius + 273;
	}
	public double KelvinParaCelsius(double Kelvin){
		return Kelvin - 273;
	}
	
	
	
	
	
	
}
